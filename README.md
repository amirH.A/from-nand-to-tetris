# From Nand To Tetris

My solutions to the from nand to tetris course: https://www.nand2tetris.org/software

This beginner friendly course takes students through some of the fundamentals of
CS. Going from the first principals to the very end in a cohesive manner is
something I've been meaning to try out for a long time and this is a great
course for doing just this!
